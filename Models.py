from sqlalchemy.ext.automap import automap_base
from sqlalchemy.orm import Session
from sqlalchemy import create_engine


class Models:

    def __init__(self):
        self.Base = automap_base()
        self.engine = create_engine('mysql://argobp:b@tt1ngPract1c3@ec2-52-26-38-121.us-west-2.compute.amazonaws.com:3306/argobp')
        self.Base.prepare(self.engine, reflect=True)
        self.Submissions = self.Base.classes.submissions
        self.session = Session(self.engine)

    def get_session(self):
        return self.session

    def get_submissions(self):
        return self.Submissions
