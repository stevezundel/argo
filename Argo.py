from flask import Flask
from flask import render_template
from Models import Models
from sqlalchemy.orm import load_only

app = Flask(__name__)


@app.route('/data_form/<submission>')
def data_form(submission):
    db = Models()
    session = db.get_session()
    submission_data = session.query(db.get_submissions()).filter(db.get_submissions().submission_no == submission).first()
    return render_template('DataForm.html', submission_info=submission_data)


@app.route('/search')
def search():
    db = Models()
    session = db.get_session()
    submissions = session.query(db.get_submissions()).options(load_only("AGENT_CODE", "submission_no", "insured_name", "OPERATING_UNIT_NAME")).limit(100)
    return render_template('Search.html', submissions=submissions)

if __name__ == '__main__':
    app.run(host='0.0.0.0')